#include <stdio.h>
#include <string.h>
//standard input output
//标准输入输出

//int main()
//{
//	printf("abcdef\n");
//	printf("%d\n",100);//%d是打印整型的
//	printf("%c\n",'w');//%c是打印字符的
//	printf("%f\n", 3.5f);//%f是打印小数；
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 32; i <= 127; i++)
//	{
//		if (i % 16 == 0)
//			printf("\n"); 
//		printf("%c ", i);
//	}
//	return 0;
//} 



//int main()
//{
//	'a';//单引号表示的字符
//	'w';
//	"abcdef";//双引号表示字符串
//	"";//空字符串
//
//
//	return 0;
//}


//int main()
//{
//	printf("hello\n");//打印字符串的方法一
//	printf("%s" , "hello");//打印字符串的方法二
//	return 0;
//}

//
//int main()
//{
//	//char arr1[3];//数组-一组数据，arr1[3]表示可以放3个数据
//	//char arr2[6];
//	char arr1[] = "abc";
//	char arr2[] = { 'a' , 'b' , 'c', '\0' };
//	char arr3[] = "abc\0defg";
//	printf("%s\n",  arr1 );
//	printf("%s\n",  arr2 );
//	printf("%s\n", arr3);
//	return 0;
// }

//int main()
//{
//	printf("\a");
//	return 0;
//}



int main()
{
    char arr[] = { 'b', 'i', 't' };
    printf("%d\n", strlen(arr));
    return 0;
}